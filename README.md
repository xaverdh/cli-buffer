## What it is

A small toy application that serves as an experiment and example on how to build multi-line command line interfaces in Haskell using the terminal library.

## Why?

I was frustrated that many cli interfaces don't have good multi-line input, and wanted to understand what it takes to implement this.

## What I learned

There is an inherent flaw in the design of terminal-program interaction in the presence of terminal side reflow. The terminal may resize the buffer asynchronously at any time, but the program has to know the correct buffer dimensions in order to move the cursor correctly while drawing. When resizing the terminal too fast, this race condition can be observed as duplicate lines of text above the input.
