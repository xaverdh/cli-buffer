{-# language LambdaCase #-}
module Data.Focused (
  Focused(..)
, emptyFocused
, pushL
, pushR
, insertL
, insertR
, peekL
, peekR
, lengthL
, lengthR
, length
, popL
, popR
, moveL
, moveR
, fromListFocusL
, fromListFocusR
, toList
, split
, mapParts
, transferOffset
) where

import Prelude hiding (length)
import Data.Natural
import Data.Foldable (foldl')

data Focused a = Focused [a] Natural [a] Natural
  deriving (Eq,Ord,Show)

emptyFocused :: Focused a
emptyFocused = Focused [] 0 [] 0

pushL :: a -> Focused a -> Focused a
pushL a (Focused ls ll rs lr) = Focused (a:ls) (ll+1) rs lr

pushR :: a -> Focused a -> Focused a
pushR a (Focused ls ll rs lr) = Focused ls ll (a:rs) (lr+1)

insertL :: [a] -> Focused a -> Focused a
insertL = flip $ foldl' (flip pushL)

insertR :: [a] -> Focused a -> Focused a
insertR = flip $ foldr pushR

peekL :: Focused a -> Maybe a
peekL = \case
  Focused (a:_) _ _ _ -> Just a
  _ -> Nothing

peekR :: Focused a -> Maybe a
peekR = \case
  Focused _ _ (a:_) _ -> Just a
  _ -> Nothing

lengthL :: Focused a -> Natural
lengthL (Focused _ ll _ _) = ll

lengthR :: Focused a -> Natural
lengthR (Focused _ _ _ lr) = lr

length :: Focused a -> Natural
length f = lengthL f + lengthR f

popL :: Focused a -> Maybe (a,Focused a)
popL = \case
  Focused (a:ls) ll rs lr -> Just . (a,) $ Focused ls (ll-1) rs lr
  _ -> Nothing

popR :: Focused a -> Maybe (a,Focused a)
popR = \case
  Focused ls ll (a:rs) lr -> Just . (a,) $ Focused ls ll rs (lr-1)
  _ -> Nothing

moveL :: Focused a -> Maybe (Focused a)
moveL f = uncurry pushR <$> popL f

moveR :: Focused a -> Maybe (Focused a)
moveR f = uncurry pushL <$> popR f

fromListFocusR :: [a] -> Focused a
fromListFocusR = foldl' (flip pushL) emptyFocused

fromListFocusL :: [a] -> Focused a
fromListFocusL = foldr pushR emptyFocused

toList :: Focused a -> [a]
toList (Focused ls _ rs _) = reverse ls ++ rs

split :: Focused a -> (Focused a,Focused a)
split (Focused ls ll rs lr) = ( Focused ls ll [] 0, Focused [] 0 rs lr)

mapParts :: (a -> b) -> (a -> b) -> Focused a -> Focused b
mapParts f1 f2 (Focused ls ll rs lr) = Focused (f1 <$> ls) ll (f2 <$> rs) lr 

transferOffset :: Focused a -> Focused a -> (Focused a, Focused a)
transferOffset f1 f2 = case (moveL f1,moveR f2) of
  (Just f1',Just f2') -> transferOffset f1' f2'
  _ -> (f1,f2)

