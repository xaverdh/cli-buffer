{-# language LambdaCase #-}
{-# language OverloadedRecordDot #-}
module System.Cli.InputLoop (
  readInput
) where

import System.Cli.Buffer qualified as B
import Data.List qualified as Lst
import System.Cli.Input qualified as CliI
import System.Cli.Input

import System.IO
import System.Terminal qualified as T
import System.Terminal (MonadTerminal)

import Control.Monad.State.Class
import Control.Monad.State
import Control.Monad.Extra


-- for debugging
tracePoint :: (Show v,MonadIO m) => v -> InputStateT m ()
tracePoint v = whenM (gets debug) $ do
  liftIO . hPutStrLn stderr $ show v

inputLoop :: (MonadIO m,MonadTerminal m) => InputStateT m ()
inputLoop = do
  get >>= tracePoint
  lift T.awaitEvent >>= \case
    Left interrupt -> do
      CliI.resetInput
      lift $ do
        T.putStringLn $ show interrupt
        T.flush
      inputLoop
    Right (T.KeyEvent (T.CharKey 'D') mods) | mods == T.ctrlKey -> pure ()
    Right ev -> flip (*>) inputLoop $ case ev of
      T.KeyEvent key mods -> case key of
        T.CharKey c -> case c of
          'L' | mods == T.ctrlKey -> do
            modify $ \st -> st{ debug = True }
          'I' | mods == T.ctrlKey -> do
            CliI.insertChar ' '
          'J' | mods == T.ctrlKey -> do
            setCtrlJ True -- see https://github.com/lpeterse/haskell-terminal/issues/17
          _ -> CliI.insertChar c
        T.SpaceKey -> pure ()
        T.BackspaceKey -> CliI.backspace
        T.DeleteKey -> CliI.deleteKey
        T.ArrowKey direction -> do
          CliI.arrowKey direction
        T.EnterKey -> do
          whenM haveCtrlJ $ do
            CliI.breakLine
            setCtrlJ False
        _ -> pure ()
      T.WindowEvent w | w == T.WindowSizeChanged -> do
        CliI.updateLineWidth
      T.DeviceEvent d | T.CursorPositionReport p <- d -> do
        liftIO $ hPutStrLn stderr $ show p
      _ -> pure ()

readInput :: IO String
readInput = do
  (_,st) <- CliI.doInputLoop inputLoop
  pure . Lst.intercalate "\n" $ B.toLines st.buf

