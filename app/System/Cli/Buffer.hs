{-# language LambdaCase #-}
{-# language OverloadedRecordDot, NoFieldSelectors #-}
module System.Cli.Buffer (
  Buffer(..)
, emptyBuffer
, Position(..)
, getPosition
, getLineOffset
, currentLineHeight
, changeWidthTo
, currentLine
, linesPreceding
, linesSucceeding
, toLines
, moveLeft
, moveRight
, moveUp
, moveDown
, insert
, deleteLeft
, deleteRight
, peekLeft
, peekRight
, breakLine
, clearBuffer
) where

import Prelude hiding (lines)
import Data.Natural
import Data.Bifunctor
import Data.Foldable (foldl')
import Data.Focused qualified as F
import Data.List (genericLength)

import Control.Monad.Identity
import Control.Applicative


type Lines a = F.Focused ([a],Natural)

linesToList :: Lines a -> [[a]]
linesToList = fmap fst . F.toList


data Buffer a = Buffer
  { lines :: Lines a
  , currLine :: F.Focused a
  , width :: Natural }
  deriving (Eq,Ord,Show)

applyDelta :: Integer -> Lines a -> Lines a
applyDelta deltaY
  | deltaY == 0 = id
  | otherwise   = F.mapParts id (second addDelta)
  where
    addDelta :: Natural -> Natural
    addDelta y = fromIntegral $ toInteger y + deltaY

alterCurrentLineM :: Monad m => (Buffer a -> m (F.Focused a)) -> Buffer a -> m (Buffer a)
alterCurrentLineM f b = do
  newLine <- f b
  let b' = b{currLine = newLine}
  let h' = currentLineHeight b'
  pure $ b'{ lines = applyDelta (toInteger h' - toInteger h) b'.lines }
  where
    h = currentLineHeight b

alterCurrentLine :: (Buffer a -> F.Focused a) -> Buffer a -> Buffer a
alterCurrentLine f = runIdentity . alterCurrentLineM (pure . f)

emptyBuffer :: Natural -> Buffer a
emptyBuffer w = Buffer F.emptyFocused F.emptyFocused w

currentLine :: Buffer a -> [a]
currentLine = F.toList . (.currLine)

linesPreceding :: Buffer a -> [[a]]
linesPreceding = linesToList . fst . F.split . (.lines)

linesSucceeding :: Buffer a -> [[a]]
linesSucceeding = linesToList . snd . F.split . (.lines)

toLines :: Buffer a -> [[a]]
toLines b = linesPreceding b ++ (currentLine b : linesSucceeding b)

pushLeftWithLength :: Natural -> [a] -> Buffer a -> Buffer a
pushLeftWithLength k ln b =
  b{lines = F.pushL (ln,offY + deltaY)
          $ applyDelta (toInteger deltaY) b.lines }
  where
    deltaY = 1 + fst (k `divMod` b.width)
    offY = getLineOffset b

pushLeft :: [a] -> Buffer a -> Buffer a
pushLeft ln = pushLeftWithLength (genericLength ln) ln

pushRightWithLength :: Natural -> [a] -> Buffer a -> Buffer a
pushRightWithLength k ln b = 
  b{lines = F.pushR (ln,offY + deltaY)
          $ applyDelta (toInteger deltaY) b.lines }
  where
    deltaY = 1 + fst (k `divMod` b.width)
    offY = getLineOffset b + currentLineHeight b

pushRight :: [a] -> Buffer a -> Buffer a
pushRight ln = pushRightWithLength (genericLength ln) ln

popLeft :: Buffer a -> Maybe ([a],Buffer a)
popLeft b = do
  ((l,offOld),ls) <- F.popL b.lines
  let offNew = getLineOffset b{lines = ls}
  pure $ ( l, b{lines = applyDelta
                (toInteger offNew - toInteger offOld)
                ls } )

popRight :: Buffer a -> Maybe ([a],Buffer a)
popRight b = do
  ((l,offOld),ls) <- F.popR b.lines
  pure $ ( l, b{lines = applyDelta
                (toInteger offY - toInteger offOld)
                ls } )
  where
    offY = getLineOffset b + currentLineHeight b
 
moveLeft :: Buffer a -> Maybe (Buffer a)
moveLeft b = case F.moveL oldLine of
  Just l -> Just $ b{currLine = l}
  Nothing -> do
    ((l,_),ls) <- F.popL b.lines
    let newLine = F.fromListFocusR l
    Just $ b{ lines = F.pushR (F.toList oldLine,offY + h) ls
            , currLine = newLine }
  where
    oldLine = b.currLine
    h = currentLineHeight b
    offY = getLineOffset b

moveRight :: Buffer a -> Maybe (Buffer a)
moveRight b = case F.moveR oldLine of
  Just l -> Just $ b{currLine = l}
  Nothing -> do
    ((l,_),ls) <- F.popR b.lines
    let newLine = F.fromListFocusL l
    Just $ b{ lines = F.pushL (F.toList oldLine,offY + h) ls
            , currLine = newLine }
  where
    oldLine = b.currLine
    h = currentLineHeight b
    offY = getLineOffset b

moveUp :: Buffer a -> Maybe (Buffer a)
moveUp b = do
  ((l,_),ls) <- F.popL b.lines
  let (old,new) = F.transferOffset (b.currLine) $ F.fromListFocusL l
  Just $ b{ lines = F.pushR (F.toList old,offY + h) ls
          , currLine = new }
  where
    h = currentLineHeight b
    offY = getLineOffset b

moveDown :: Buffer a -> Maybe (Buffer a)
moveDown b = do
  ((l,_),ls) <- F.popR b.lines
  let (old,new) = F.transferOffset (b.currLine) $ F.fromListFocusL l
  Just $ b{ lines = F.pushL (F.toList old,offY + h) ls
          , currLine = new }
  where
    h = currentLineHeight b
    offY = getLineOffset b

insert :: a -> Buffer a -> Buffer a
insert a = alterCurrentLine $ F.pushL a . (.currLine)

deleteRight :: Buffer a -> Maybe (Buffer a,Bool)
deleteRight b = (,False) <$> delteMiddle b <|> (,True) <$> deleteOuter b
  where
    delteMiddle = alterCurrentLineM $ \b -> do
      (_,newLine) <- F.popR $ b.currLine
      Just newLine

    deleteOuter b = do
      (l,b') <- popRight b
      Just $ alterCurrentLine (F.insertR l . (.currLine)) b'

deleteLeft :: Buffer a -> Maybe (Buffer a,Bool)
deleteLeft b = (,False) <$> deleteMiddle b <|> (,True) <$> deleteOuter b
  where
    deleteMiddle = alterCurrentLineM $ \b -> do
      (_,newLine) <- F.popL $ b.currLine
      Just newLine

    deleteOuter b = do
      (l,b') <- popLeft b
      Just $ alterCurrentLine (F.insertL l . (.currLine)) b'

peekLeft :: Buffer a -> Maybe a
peekLeft b = F.peekL $ b.currLine

peekRight :: Buffer a -> Maybe a
peekRight b = F.peekR $ b.currLine

breakLine :: Buffer a -> Buffer a
breakLine b =
  pushLeftWithLength (F.lengthL l) (F.toList l)
  $ alterCurrentLine (const r) b
  where
    (l,r) = F.split $ b.currLine


data Position = Position !Natural !Natural
  deriving (Eq,Ord,Show)

getPosition :: Buffer a -> Position
getPosition b = Position x (offY + overflowY)
  where
    offY = getLineOffset b
    (overflowY,x) = F.lengthL (b.currLine) `divMod` b.width

getLineOffset :: Buffer a -> Natural
getLineOffset b = case F.popL b.lines of
  Just ((_,offY),_) -> offY
  Nothing -> 0

currentLineHeight :: Buffer a -> Natural
currentLineHeight b = 1 + fst (F.length (b.currLine) `divMod` b.width)

changeWidthTo :: Natural -> Buffer a -> Buffer a
changeWidthTo w b = 
  flip (foldl' $ flip pushLeft) (linesToList bl)
  . flip (foldr pushRight) (linesToList br)
  $ (emptyBuffer w){currLine = b.currLine}
  where
    (bl,br) = F.split b.lines

clearBuffer :: Buffer a -> Buffer a
clearBuffer b = emptyBuffer b.width

