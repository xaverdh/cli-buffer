{-# language LambdaCase #-}
{-# language OverloadedRecordDot #-}
module System.Cli.Input (
  InputState(..)
, emptyInputState
, setupInputState
, initScreen

, InputStateT
, doInputLoop
, haveCtrlJ
, setCtrlJ

, updateLineWidth
, insertChar
, breakLine
, backspace
, deleteKey
, arrowKey
, resetInput
) where

import Prelude hiding (putChar)

import System.Cli.Buffer qualified as B
import System.Cli.Cursor
import System.Terminal qualified as T
import System.Terminal.Internal qualified as TI
import System.Terminal (MonadTerminal)
import System.IO hiding (putChar)
import Control.Monad.Catch (MonadMask)
import Data.List qualified as Lst

import Data.Natural

import Control.Monad.Trans.Class
import Control.Monad.IO.Class
import Control.Monad.State
import Control.Monad.Extra

data InputState = InputState
 { buf :: B.Buffer Char
 , ctrlJ :: Bool
 , debug :: Bool
 , posX :: Int
 , posY :: Int }
 deriving (Eq,Ord,Show)

emptyInputState :: Natural -> InputState
emptyInputState w = InputState (B.emptyBuffer w) False False 0 0

putBuf :: MonadState InputState m
  => B.Buffer Char -> m ()
putBuf b = get >>= \st -> put st{ buf = b }

haveCtrlJ :: MonadState InputState m => m Bool
haveCtrlJ = gets ctrlJ

setCtrlJ :: MonadState InputState m => Bool -> m ()
setCtrlJ b = modify $ \st -> st{ ctrlJ = b }


setupInputState :: MonadTerminal m => m InputState
setupInputState = do
  T.setAutoWrap False
  w <- getLineWidth
  pure $ emptyInputState w

initScreen :: (MonadIO m,MonadTerminal m) => m ()
initScreen = do
  liftIO . hSetBuffering stdout $ BlockBuffering (Just $ 8^3)
  T.eraseInDisplay T.EraseForward
  T.flush

newtype InputStateT m a = InputStateT ( StateT InputState m a )
  deriving
  ( Functor
  , Applicative
  , Monad
  , MonadIO
  , MonadTrans
  , MonadState InputState )

runInputStateT :: Monad m
  => InputStateT m a -> InputState -> m (a,InputState)
runInputStateT (InputStateT inpLoop) st = inpLoop `runStateT` st

doInputLoop :: (MonadIO m,MonadMask m)
  => InputStateT (T.TerminalT TI.LocalTerminal m) a
  -> m (a,InputState)
doInputLoop inpLoop = T.withTerminal . T.runTerminalT $ do
  st <- setupInputState
  initScreen
  runInputStateT inpLoop st

instance MonadTerminal m => HasTerminal (InputStateT m) where
  liftTerminal f = lift f

instance Monad m => HasCursorPosition (InputStateT m) where
  getCursorPosition = (,) <$> gets posX <*> gets posY
  setCursorPosition (x,y) = get >>= \st -> put $ st{posX = x, posY = y}


reDraw :: MonadTerminal m => InputStateT m ()
reDraw = bracketCursor $ do
  resetCursor
  b <- gets buf
  putLines $ B.toLines b

reDrawFromCurrentLine :: MonadTerminal m => InputStateT m ()
reDrawFromCurrentLine = bracketCursor $ do
  b <- gets buf
  let offsetY = B.getLineOffset b
  adjustCursorPosition $ B.Position 0 offsetY
  putLines $ B.currentLine b : B.linesSucceeding b

reDrawCurrentLine :: MonadTerminal m => InputStateT m ()
reDrawCurrentLine = bracketCursor $ do
  b <- gets buf
  let offsetY = B.getLineOffset b
  adjustCursorPosition $ B.Position 0 offsetY
  putLines [B.currentLine b]

putLines :: (MonadIO m,T.MonadTerminal m)
  => [String] -> InputStateT m ()
putLines lns = do
  b <- gets buf
  lift $ T.eraseInDisplay T.EraseForward
  putString . unlines $ wrap b.width lns
  where
    wrap :: Natural -> [String] -> [String]
    wrap w ls = do
      l <- ls
      splitLine w l

    splitLine :: Natural -> [a] -> [[a]]
    splitLine w l =
      let (l',rst) = Lst.splitAt (fromIntegral w) l
       in (l':) $ case rst of
        [] -> if Lst.genericLength l' == w then [[]] else []
        _ -> splitLine w rst

adjustCursorPosition :: (MonadIO m,MonadTerminal m)
  => B.Position -> InputStateT m ()
adjustCursorPosition (B.Position x' y') = do
  (x,y) <- getCursorPosition
  moveCursor (fromIntegral x' - x, fromIntegral y' - y)

syncCursorPosition :: MonadTerminal m => InputStateT m ()
syncCursorPosition = do
  b <- gets buf
  adjustCursorPosition $ B.getPosition b

getLineWidth :: MonadTerminal m => m Natural
getLineWidth = adjustWidth <$> T.getLineWidth
  where
    adjustWidth w = fromIntegral w `monus` 2
    {-add some space to reduce artefacts from reflow on some terminals-}

moveBuffer :: T.Direction -> B.Buffer a -> Maybe (B.Buffer a)
moveBuffer d = case d of
  T.Upwards -> B.moveUp
  T.Downwards -> B.moveDown
  T.Leftwards -> B.moveLeft
  T.Rightwards -> B.moveRight

updateLineWidth :: MonadTerminal m => InputStateT m ()
updateLineWidth = do
  w <- lift getLineWidth
  modify $ \st -> st{buf = B.changeWidthTo (fromIntegral w) st.buf}
  reDraw
  syncCursorPosition
  lift T.flush


heightChanged :: B.Buffer a -> B.Buffer a -> Bool
heightChanged b b' = B.currentLineHeight b' /= B.currentLineHeight b

insertChar :: MonadTerminal m => Char -> InputStateT m ()
insertChar c = do
  b <- gets buf
  let b' = B.insert c b
  putBuf b'
  if heightChanged b b' then do
    reDrawFromCurrentLine
    syncCursorPosition
  else if B.currentLineHeight b' > 1 then do
    reDrawCurrentLine
    syncCursorPosition
  else do
    whenJust (B.peekRight b) . const . lift $ T.insertChars 1
    {-^^insert middle-}
    putChar c
  lift T.flush

breakLine :: MonadTerminal m => InputStateT m ()
breakLine = do
  gets buf >>= putBuf . B.breakLine
  lift $ T.eraseInLine T.EraseForward
  putLn
  reDrawFromCurrentLine
  lift T.flush

backspace :: MonadTerminal m => InputStateT m ()
backspace = do
  b <- gets buf
  whenJust (B.deleteLeft b) $ \(b',merge) -> do
    putBuf b'
    if merge || heightChanged b b' then do
      reDrawFromCurrentLine
      syncCursorPosition
    else if B.currentLineHeight b' > 1 then do
      reDrawCurrentLine
      syncCursorPosition
    else do
      moveCursorBackward 1
      lift $ T.deleteChars 1
    lift T.flush

deleteKey :: MonadTerminal m => InputStateT m ()
deleteKey = do
  b <- gets buf
  whenJust (B.deleteRight b) $ \(b',merge) -> do
    putBuf b'
    if merge || heightChanged b b' then do
      reDrawFromCurrentLine
      syncCursorPosition
    else if B.currentLineHeight b' > 1 then do
      reDrawCurrentLine
      syncCursorPosition
    else
      lift $ T.deleteChars 1
    lift T.flush

arrowKey :: MonadTerminal m => T.Direction -> InputStateT m ()
arrowKey direction = do
  b <- gets buf
  whenJust (moveBuffer direction b) $ \b' -> do
    adjustCursorPosition $ B.getPosition b'
    putBuf b'
    lift T.flush

resetInput :: MonadTerminal m => InputStateT m ()
resetInput = do
  gets buf >>= putBuf . B.clearBuffer
  resetCursor
  lift $ do
    T.eraseInDisplay T.EraseForward
    T.flush

