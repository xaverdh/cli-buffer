{-# language ImpredicativeTypes #-}
module System.Cli.Cursor (
  HasTerminal(..)
, HasCursorPosition(..)
, moveCursorForward
, moveCursorBackward
, moveCursorUp
, moveCursorDown
, resetCursorX
, resetCursorY
, resetCursor
, moveCursorVertically
, moveCursorHorizontally
, moveCursor
, bracketCursor
, putLn
, putChar
, putString
) where

import Prelude hiding (putChar)
import System.Terminal qualified as T
import Control.Monad.Extra
import Data.Kind (Type)
import Data.List as Lst

-- import Capability.State

{- All functionality here assumes that (automatic) line wrapping is *off*! -}

class HasCursorPosition m where
  getCursorPosition :: m (Int,Int)
  setCursorPosition :: (Int,Int) -> m ()

class HasTerminal (m :: Type -> Type) where
  liftTerminal :: forall a. (forall m'. T.MonadTerminal m' => m' a) -> m a

type CPTM m = (HasCursorPosition m,HasTerminal m,Monad m)  

shiftX :: (HasCursorPosition m,Monad m) => Int -> m Bool
shiftX x = do
  (px,py) <- getCursorPosition
  let clip = px + x < 0
  setCursorPosition (if clip then 0 else px + x,py)
  pure $ not clip

shiftY :: (HasCursorPosition m,Monad m) => Int -> m Bool
shiftY y = do
  (px,py) <- getCursorPosition
  let clip = py + y < 0
  setCursorPosition (px,if clip then 0 else py + y)
  pure $ not clip

moveCursorUp :: CPTM m => Int -> m ()
moveCursorUp y = whenM (shiftY (-y)) . liftTerminal $ (T.moveCursorUp y :: T.MonadTerminal m' => m' ())

moveCursorDown :: CPTM m => Int -> m ()
moveCursorDown y = whenM (shiftY y) . liftTerminal $ T.moveCursorDown y

moveCursorBackward :: CPTM m => Int -> m ()
moveCursorBackward x = whenM (shiftX (-x)) . liftTerminal $ T.moveCursorBackward x

moveCursorForward :: CPTM m => Int -> m ()
moveCursorForward x = whenM (shiftX x) . liftTerminal $ T.moveCursorForward x

resetCursorX :: CPTM m => m ()
resetCursorX = do
  (x,_) <- getCursorPosition
  moveCursorBackward x

resetCursorY :: CPTM m => m ()
resetCursorY = do
  (_,y) <- getCursorPosition
  moveCursorUp y

resetCursor :: CPTM m => m ()
resetCursor = resetCursorY >> resetCursorX

moveCursorVertically :: CPTM m => Int -> m ()
moveCursorVertically y
  | y == 0 = pure ()
  | y < 0 = moveCursorUp (-y)
  | otherwise = moveCursorDown y

moveCursorHorizontally :: CPTM m => Int -> m ()
moveCursorHorizontally x
  | x == 0 = pure ()
  | x < 0 = moveCursorBackward (-x)
  | otherwise = moveCursorForward x

moveCursor :: CPTM m => (Int,Int) -> m ()
moveCursor (x,y) = do
  moveCursorVertically y
  moveCursorHorizontally x

bracketCursor :: CPTM m => m a -> m a
bracketCursor action = do
  (x,y) <- getCursorPosition
  r <- action
  (x',y') <- getCursorPosition
  moveCursor (x-x',y-y')
  pure r

putLn :: CPTM m => m ()
putLn = do
  liftTerminal T.putLn
  _ <- shiftY 1
  (x,_) <- getCursorPosition
  void $ shiftX (-x)

putChar :: CPTM m => Char -> m ()
putChar c = if c == '\n' then putLn else do
  liftTerminal $ T.putChar c
  void $ shiftX 1

putString :: CPTM m => String -> m ()
putString s = sequence_
  . Lst.intersperse putLn
  $ putStringWithoutNewline <$> lines s

putStringWithoutNewline :: CPTM m => String -> m ()
putStringWithoutNewline s = do
  liftTerminal $ T.putString s
  void . shiftX $ length s

