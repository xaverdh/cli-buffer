{ nixpkgs ? import <nixpkgs> {}, compiler ? "ghc92", doBenchmark ? false, o2 ? true }:

let

  inherit (nixpkgs) pkgs;

  f = { mkDerivation, base, exceptions, extra, lib, mtl
      , natural-numbers, terminal, transformers
      }:
      mkDerivation {
        pname = "cli-buffer";
        version = "0.1.0.0";
        src = ./.;
        isLibrary = false;
        isExecutable = true;
        executableHaskellDepends = [
          base exceptions extra mtl natural-numbers terminal transformers
        ];
        description = "A simple multi-line cli application";
        license = lib.licenses.gpl3Plus;
        mainProgram = "cli-buffer";
      };

  haskellPackages = if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  variant = if o2 then pkgs.haskell.lib.compose.appendConfigureFlag "-O2" else pkgs.lib.id
    (if doBenchmark then pkgs.haskell.lib.doBenchmark else pkgs.lib.id);

  drv = variant (haskellPackages.callPackage f {});

in

  if pkgs.lib.inNixShell then drv.env else drv
